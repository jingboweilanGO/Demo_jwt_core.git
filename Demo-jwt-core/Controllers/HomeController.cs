﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Demo_jwt_core2.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController  : ControllerBase
    {
            [HttpGet]
            [Route("api/value1")]
            public ActionResult<IEnumerable<string>> Get()
            {
                return new string[] { "value1", "value1" };
            }

            [HttpGet]
            [Route("api/value2")]
            [Authorize]
            public ActionResult<IEnumerable<string>> Get2()
            {
                return new string[] { "value2", "value2" };
            }
    }
}
